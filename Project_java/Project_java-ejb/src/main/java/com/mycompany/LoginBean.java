/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.project_java.services.ejb_remote.LoginRemote;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;

/**
 *
 * @author exomat
 */
@Stateless
@RolesAllowed("Customer")
public class LoginBean implements LoginRemote {

    @Override
    public boolean is_logged() {
        return true; 
    }
    
}
