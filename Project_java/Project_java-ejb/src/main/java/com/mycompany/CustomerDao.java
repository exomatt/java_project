/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.project_java.services.CustomerDTO;
import com.mycompany.project_java.services.MovieDTO;
import com.mycompany.project_java.services.ejb_remote.CustomerRemote;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author exomat
 */
@Stateless
public class CustomerDao implements CustomerRemote {
    @PersistenceContext(unitName = "PU")
    private EntityManager em;
    
    public void save(CustomerDTO customer) {
        em.persist(customer);
    }

    public void remove(Long id) {
        em.remove(em.getReference(CustomerDTO.class, id));
    }
    
    public List<CustomerDTO> findByLastName(String lastName) {
        TypedQuery<CustomerDTO> q = em.createNamedQuery("CustomerDTO.findByLastName", CustomerDTO.class);
        q.setParameter("lastName", lastName);
         return q.getResultList();
    }

    @Override
    public List<CustomerDTO> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CustomerDTO findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(CustomerDTO o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
