/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.project_java.services.GroupDTO;
import com.mycompany.project_java.services.ejb_remote.GroupRemote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author exomat
 */
@Stateless
public class GroupDao implements GroupRemote{

   @PersistenceContext(unitName = "PU")
    private EntityManager em;

    @Override
    public void save(GroupDTO group) {
        em.persist(group);
    }
    
    @Override
    public void update(GroupDTO group) {
        em.merge(group);
    }
    
}
