/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.project_java.services.MovieDTO;
import com.mycompany.project_java.services.ejb_remote.MovieRemote;
import java.util.List;
import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author exomat
 */
@DataSourceDefinition(
        name = "java:global/dvdpool",
        className = "org.apache.derby.jdbc.ClientDataSource",
        minPoolSize = 1,
        initialPoolSize = 1,
        portNumber = 1527,
        serverName = "localhost",
        user = "admin",
        password = "admin",
        databaseName = "APP",
        properties = {"connectionAttributes=;create=true"}
)
@Stateless
public class MovieDao implements MovieRemote{
    @PersistenceContext(unitName = "PU")
    private EntityManager em;
    
    public void save(MovieDTO movie) {
        em.persist(movie);
    }

    public void remove(Long id) {
        em.remove(em.getReference(MovieDTO.class, id));
    }

    public List<MovieDTO> findAll() {
        TypedQuery<MovieDTO> q = em.createNamedQuery("MovieDTO.findAll", MovieDTO.class);
        return q.getResultList();
    }

//    public void update(Long id, String title, String director) {
//        TypedQuery<MovieDTO> q = em.createNamedQuery("MovieDTO.update", MovieDTO.class);
//        q.setParameter("pid", id);
//        q.setParameter("ptitle", title);
//        q.setParameter("pdirector", director);
//        em.getTransaction().begin();
//        q.executeUpdate();
//        em.getTransaction().commit();
//    }

    @Override
    public MovieDTO findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(MovieDTO o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
