/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.project_java.services.CustomerDTO;
import com.mycompany.project_java.services.OrderDTO;
import com.mycompany.project_java.services.ejb_remote.OrderRemote;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author exomat
 */
@Stateless
public class OrderDao implements OrderRemote{
     @PersistenceContext(unitName = "PU")
    private EntityManager em;
    
    public void save(OrderDTO order) {
        em.persist(order);
    }

    public void remove(Long id) {
        em.remove(em.getReference(OrderDTO.class, id));
    }
    
    public List<OrderDTO> findAll() {
        TypedQuery<OrderDTO> q = em.createNamedQuery("OrderDTO.findAll", OrderDTO.class);
        return q.getResultList();
    }

    @Override
    public OrderDTO findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(OrderDTO o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
