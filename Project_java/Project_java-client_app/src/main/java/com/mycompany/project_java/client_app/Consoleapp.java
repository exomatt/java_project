/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_java.client_app;

import com.mycompany.project_java.services.ejb_remote.CustomerRemote;
import com.mycompany.project_java.services.ejb_remote.GroupRemote;
import com.mycompany.project_java.services.ejb_remote.MovieRemote;
import com.mycompany.project_java.services.ejb_remote.OrderRemote;
import java.net.URL;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author exomat
 */
public class Consoleapp {
    public static void main(String[] args) throws NamingException{
        URL url = Consoleapp.class.getResource("/appclientlogin.conf");
        System.setProperty("java.security.auth.login.config", url.toExternalForm());
        InitialContext ic = new InitialContext();
        
        CustomerRemote customer_ejb = (CustomerRemote) ic.lookup("java:global/Project_java-ear/Project_java-ejb-1.0-SNAPSHOT/CustomerDao!com.mycompany.project_java.services.ejb_remote.CustomerRemote");
        MovieRemote movie_ejb = (MovieRemote) ic.lookup("java:global/Project_java-ear/Project_java-ejb-1.0-SNAPSHOT/MovieDao!com.mycompany.project_java.services.ejb_remote.MovieRemote");
        OrderRemote order_ejb = (OrderRemote) ic.lookup(" java:global/Project_java-ear/Project_java-ejb-1.0-SNAPSHOT/OrderDao!com.mycompany.project_java.services.ejb_remote.OrderRemote");
        GroupRemote group_ejb = (GroupRemote) ic.lookup("[java:global/Project_java-ear/Project_java-ejb-1.0-SNAPSHOT/GroupDao!com.mycompany.project_java.services.ejb_remote.GroupRemote");
    
    }
}
