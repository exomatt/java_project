/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_java.services.ejb_remote;

/**
 *
 * @author exomat
 */

import com.mycompany.project_java.services.OrderDTO;
import javax.ejb.Remote;

@Remote
public interface OrderRemote extends AbstractRemote<OrderDTO> {
    
}
