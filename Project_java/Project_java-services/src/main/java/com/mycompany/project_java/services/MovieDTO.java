    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_java.services;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author exomat
 */
@Entity
@NamedQueries({
@NamedQuery(name = "MovieDTO.findByTitle", query="select c from MovieDTO c where c.title= :title"),
@NamedQuery(name = "MovieDTO.findAll", query="select m from MovieDTO m"),       
//@NamedQuery(name = "MovieDTO.update", query = "update MovieDTO  set title = :ptitle , set director= :pdirector where id = :pid"),
}
)
@Table (name="Movie", schema="APP")
public class MovieDTO implements Serializable  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String director;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "["+this.getId()+","+title+","+director+"]";
    }
}
