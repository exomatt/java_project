/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_java.services.ejb_remote;

import java.util.List;

/**
 *
 * @author exomat
 * @param <T>
 */
public interface AbstractRemote <T> {
    void save(T o);
    List<T> findAll();
    void remove(Long id);
    T findById(Long id);
    void update(T o);
    
}
