/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_java.services.ejb_remote;

import com.mycompany.project_java.services.GroupDTO;
import javax.ejb.Remote;

/**
 *
 * @author exomat
 */
@Remote
public interface GroupRemote {
    public void save(GroupDTO group); 

    public void update(GroupDTO group);
}
