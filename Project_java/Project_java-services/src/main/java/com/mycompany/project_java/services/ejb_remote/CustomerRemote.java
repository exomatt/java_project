/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_java.services.ejb_remote;

import com.mycompany.project_java.services.CustomerDTO;
import javax.ejb.Remote;

/**
 *
 * @author exomat
 */
@Remote
public interface CustomerRemote extends AbstractRemote<CustomerDTO> {
    
}
